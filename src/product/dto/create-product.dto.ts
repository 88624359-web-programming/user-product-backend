import {
  IsEmpty,
  MinLength,
  Matches,
  IsNotEmpty,
  IsPositive,
} from 'class-validator';
import { isEmpty } from 'class-validator/types/decorator/decorators';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
