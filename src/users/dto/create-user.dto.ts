import { IsEmpty, MinLength, Matches, IsNotEmpty } from 'class-validator';
import { isEmpty } from 'class-validator/types/decorator/decorators';
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login: string;

  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  @MinLength(8)
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
  password: string;
}
