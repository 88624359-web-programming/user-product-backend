import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let users: User[] = [
  { id: 1, login: 'Jiheon', name: 'Beak Jiheon', password: 'Pass@1234' },
];
let lastUserId = users[users.length - 1].id + 1;

@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    const newUser: User = {
      id: lastUserId++,
      ...createUserDto,
    };
    users.push(newUser);
    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const editUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = editUser;
    return editUser;
  }

  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = users[index];
    users.splice(index, 1);
    return deletedUser;
  }

  reset() {
    users = [
      { id: 1, login: 'Jiheon', name: 'Beak Jiheon', password: 'Pass@1234' },
    ];
    return 'Reset!';
  }
}
